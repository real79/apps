FROM python:3.7
ENV PYTHONUNBUFFERED 1
RUN mkdir /apps
WORKDIR /apps
COPY . /apps
RUN pip install -r requirements.txt
EXPOSE 8000