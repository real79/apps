from django.db import models
from secrets import token_urlsafe
from django.conf import settings


def get_key_length():
    (d, m) = divmod(settings.API_KEY_NBYTES * 8, 6)
    if m > 0:
        d += 1
    return d

class AppModel(models.Model):
    name = models.CharField(max_length=100)
    key = models.CharField(max_length=get_key_length(), unique=True)

    def new_key(self):
        self.key=token_urlsafe(nbytes=settings.API_KEY_NBYTES)
        self.save()

