from rest_framework.serializers import ModelSerializer, ReadOnlyField, SerializerMethodField
from test_app.models import AppModel

class AppSerializer(ModelSerializer):
    id = SerializerMethodField('get_pk')
    key = ReadOnlyField(label='key', read_only=True)
    class Meta:
        model = AppModel
        fields = ('id', 'name', 'key')

    def create(self, validated_data):
        app=AppModel.objects.create(**validated_data)
        app.new_key()
        return app

    def get_pk(self,obj):
        return obj.pk

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.save()
        return instance
