from ast import literal_eval
from rest_framework.test import APITestCase

class TestAppView(APITestCase):
    fixtures = ['app_testing.json', ]
    def testViewList(self):
        response = self.client.get('http://testserver/api/app/')
        assert response.status_code==200
        assert response.rendered_content == b'[{"id":1,"name":"some_app","key":"some_app_key"}]'

    def testCreateApp(self):
        response = self.client.post('http://testserver/api/app/',data={'name':'new_app'})
        assert response.status_code == 201
        resp_dict = literal_eval(response.rendered_content.decode())
        assert resp_dict['name'] == 'new_app'

    def testGetApiKey(self):
        header = {'HTTP_X_API_KEY': 'some_app_key'}
        response = self.client.get('http://testserver/api/test/', None, **header)
        assert response.status_code == 200
        assert response.rendered_content == b'{"id":1,"name":"some_app","key":"some_app_key"}'

    def testUpdateApiKey(self):
        header = {'HTTP_X_API_KEY': 'some_app_key'}
        response = self.client.get('http://testserver/api/test/update-key', None, **header)
        assert response.status_code == 200
        resp_dict = literal_eval(response.rendered_content.decode())
        assert resp_dict['id']==1
        assert resp_dict['name'] == 'some_app'
        assert resp_dict['key'] != 'some_app_key'

    def testDeleteApp(self):
        header = {'HTTP_X_API_KEY': 'some_app_key'}
        response = self.client.delete('http://testserver/api/test/', None, **header)
        assert response.status_code==204
        response = self.client.get('http://testserver/api/app/1/')
        assert response.status_code==404

    def testUpdateAppName(self):
        header = {'HTTP_X_API_KEY': 'some_app_key'}
        response = self.client.post('http://testserver/api/test/', data={'name': 'new_app'}, **header)
        assert response.status_code == 200
        resp_dict = literal_eval(response.rendered_content.decode())
        assert resp_dict['name'] == 'new_app'
