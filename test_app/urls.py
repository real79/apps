from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from test_app.routers import RouteWithoutLokup
from test_app.views import AppMixinView
from test_app.views import TestAppView
from test_app.doc import Schema_view

router = DefaultRouter()
router.register(r'app', AppMixinView, basename='app-list')
custom_router = RouteWithoutLokup()
custom_router.register(r'test', TestAppView, basename='app')


urlpatterns = (router.urls)
urlpatterns += (custom_router.urls)
urlpatterns += [
    url(r'doc', Schema_view),

]
