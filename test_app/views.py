from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework import status
from test_app.models import AppModel
from test_app.serializers import AppSerializer
from rest_framework.decorators import action


class AppMixinView(GenericViewSet, ListModelMixin, RetrieveModelMixin, CreateModelMixin):
    http_method_names = ['get', 'post']
    queryset = AppModel.objects.all()
    serializer_class = AppSerializer
    flter_backends = (SearchFilter,)
    search_fields = ('key',)

class TestAppView(GenericViewSet,RetrieveModelMixin,UpdateModelMixin,DestroyModelMixin):
    http_method_names = ['get', 'post', 'delete']
    serializer_class = AppSerializer
    queryset = AppModel.objects.all()
    lookup_field = 'key'

    def retrieve(self, request, *args, **kwargs):
        """Get app info"""
        self.getKeyFromHeaders(request)
        return super(TestAppView, self).retrieve(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        """Delete app """
        self.getKeyFromHeaders(request)
        return super(TestAppView, self).destroy(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        """Update app name"""
        self.getKeyFromHeaders(request)
        return super().update(request, *args, **kwargs)

    @action(methods=['get'], detail=True, url_path='update-key', url_name='update_key')
    def set_new_key(self,request):
        """Set new key for app"""
        self.getKeyFromHeaders(request)
        app=self.get_object()
        app.new_key()
        app.save()
        self.kwargs['key']=app.key
        return self.retrieve(request,self.args,self.kwargs)

    def getKeyFromHeaders(self,request):
        if self.kwargs.get('key') is None:
            self.kwargs['key'] = request.META.get('HTTP_X_API_KEY')